<?php

declare(strict_types=1);

namespace Susanne\Hcaptcha\Exception;

use TYPO3\CMS\Core\Exception;

class MissingKeyException extends Exception
{
}
